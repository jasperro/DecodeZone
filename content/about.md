+++
comments = false
draft = false
noauthor = true
share = false
title = "About"
type = "page"
[menu.main]
weight = 5
+++

This is decodezone. We are a small team of friends that wanted to make a website.
There are weekly decode challenges where you need to figure out how to decode it.
We also have training challenges that teach you some basics about decoding.
