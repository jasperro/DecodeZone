---
author: "Sam Ringeling"
date: 2018-05-11
linktitle: "Training Challenge 2"
title: "Training Challenge 2"
authorAvatar: img/samri.jpg
---
This is the second training challenge that teaches you base64 encoding.

<!--more-->

## Training Challenge #2 - Base64 encoding

## Start
> SGVsbG8gV29ybGQh

## Hint
{{< hidden text="Hint">}}

> Read the title of the exercise!

{{</ hidden >}}

## Solution
{{< hidden text="Solution">}}

> The text is written in Base64. To decode this you can google on Base64 decoders. We suggest you use [DarkByte](https://conv.darkbyte.ru/) as it also decodes various other types of encoding (Like binary from the last exercise). If you decrypt it from Base64 to Text, you end up with the answer: Hello World!

{{</ hidden >}}
