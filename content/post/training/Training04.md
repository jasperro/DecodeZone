---
author: "Jasper Albering"
date: 2018-05-13
linktitle: "Training Challenge 4"
title: "Training Challenge 4"
authorAvatar: img/jasper.jpg
---
This is the fourth training challenge that teaches you (hexa)decimal encoding.

<!--more-->

# Training Challenge #4 - Hex & Dec
## Start

> 53 70 61 67 68 65 74 74 69

> 83 112 97 103 104 101 116 116 105

## Hint
{{< hidden text="Hint">}}

> Do you really need it? No. Da Title.

{{</ hidden >}}

## Solution
{{< hidden text="Solution">}}

> Hexadecimal and decimal encodings is binary to numbers. 01111010 is equal to 7a in hex and 122 in decimal, which equals to Z. Decimal is the normal number system, and hexadecimal uses 1-9 and A-F. The maximum decimal number hexadecimal can represent is the number 255. The answer to both start lines: Spaghetti.

{{</ hidden >}}
