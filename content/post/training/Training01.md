---
author: "Sam Ringeling"
date: 2018-05-10
linktitle: "Training Challenge 1"
title: "Training Challenge 1"
authorAvatar: img/samri.jpg
---
This is the first training challenge that teaches you binary encoding.

<!--more-->

# Training challenge #1 - binary encoding 

## Start
> 01011001 01101111 01110101 00100000 01100100 01101001 01100100 00100000 01101001 01110100 00100001

## Hint
{{< hidden text="Hint">}}

>Binary = 01000010 01101001 01101110 01100001 01110010 01111001!

{{</ hidden >}}

## Solution
{{< hidden text="Solution">}}

> The text is written in binary code. To decode this you can google on binary decoders. We suggest you use [DarkByte](https://conv.darkbyte.ru/) as it also decodes various other types of encoding. If you decrypt it from binary to Text, you end up with the answer: You did it!!

{{</ hidden >}}
