---
author: "Sam Ringeling"
date: 2018-05-12
linktitle: "Training Challenge 3"
title: "Training Challenge 3"
authorAvatar: img/samri.jpg
---
This is the third training challenge that teaches you combined encoding.

<!--more-->

# Training Challenge #3 - Combined

## Start

> 01010111 01010111 00111001 00110001 01001001 01000111 01000110 01111001 01011010 01010011 01000010 01111010 01100100 01000111 01000110 01111001 01100100 01000111 01101100 01110101 01011010 01111001 01000010 00110000 01100010 01111001 01000010 01101110 01011010 01011000 01010001 01100111 01100100 01000111 01101000 01101100 01001001 01000111 01101000 01101000 01100010 01101101 01100011 01100111 01100010 00110010 01011001 01100111 01100001 01011000 01010001 01101000

## Hint
{{< hidden text="Hint">}}

> Binary AND Base64

{{</ hidden >}}

## Solution
{{< hidden text="Solution">}}

> The string (means: line of text) that you started with is encoded in binary. You can see this because it is only 0 and 1 and it is divided in subdivisions of eight. However if you decode it, you will see that it translates to a big mess of letters. This is Base64, you can see this because it it uppercase and lowercase letters with only a few numbers. If you copy the decoded binary and paste it into a Base64 decoder, you find the solution: You are starting to get the hang of it!

{{</ hidden >}}
