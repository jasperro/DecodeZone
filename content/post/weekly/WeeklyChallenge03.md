---
author: "Sam Ringeling"
date: 2018-05-30
linktitle: "Weekly Challenge 3"
title: "Weekly Challenge 3 — Medium"
authorAvatar: img/samri.jpg
---
This is the third weekly challenge.
The difficulty is Medium.

<!--more-->

# Weekly challenge #3
## Start

{{< breakcontainer >}}
> yzhsvw hpfooh
{{< /breakcontainer >}}

## Hint

{{< hidden text="Hint">}}
> Latin or Hebrew?
{{</ hidden >}}
