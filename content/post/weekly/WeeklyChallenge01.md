---
author: "Jasper Albering"
date: 2018-05-13
linktitle: "Weekly Challenge 1"
title: "Weekly Challenge 1 — Easy"
authorAvatar: img/jasper.jpg
---
This is the first weekly challenge.
The difficulty is Easy.

<!--more-->

# Weekly challenge #1

## Start
{{< breakcontainer >}}
>ICAgCiAgIAkgCSAKICAgCQkJCSAJCiAgIAkgICAJIAkKICAgCSAJICAJCQogICAJIAkJIAkgCiAgIAkJIAkgCSAKICAgCSAgIAkJIAogICAJICAgCQkJCiAgIAkJICAgCQkKICAgCQkJCSAJIAogICAJIAkgCQkgCiAgIAkgICAJCQkKICAgCQkgIAkgIAogICAJCQkgICAgCiAgIAkJIAkgICAKICAgCQkgIAkgCiAgIAkgCSAJCSAKCiAgCSAgICAgICAgIAkgICAJICAJICAKIAogCgkgCQkJIAkgICAJIAkgIAoJCiAgCiAKCSAgICAgICAgIAkgICAJICAJICAKCiAgCQkJIAkgICAJIAkgIAogCgoKCgo=
{{< /breakcontainer >}}

## Hint
{{< hidden text="Hint" >}}
>&nbsp;
{{< /hidden >}}
