---
author: "Sam Ringeling"
date: 2018-05-23
linktitle: "Weekly Challenge 2"
title: "Weekly Challenge 2 — Medium"
authorAvatar: img/samri.jpg
---
This is the second weekly challenge.
The difficulty is Medium.

<!--more-->

# Weekly challenge #2

## Start 1
{{< breakcontainer >}}
> svdmclprdgndw
{{< /breakcontainer >}}

## Start 2
> III I V 1A:1A 2B:2B 8H:8H “sp ag he ti bo yz" UKW C M3

## Hint
{{< hidden text="Hint">}}
>WW2
{{</ hidden >}}
